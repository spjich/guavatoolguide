package com.ji.strings;

import com.google.common.base.CaseFormat;
import com.google.common.base.CharMatcher;
import com.google.common.base.Strings;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/5
 * Version:1.1.0
 */
public class StringsMy {
    public static void main(String[] args) {
        //防御式编程
        Strings.isNullOrEmpty("");//true
        Strings.nullToEmpty(null);//""
        Strings.nullToEmpty("a");//"a"
        Strings.emptyToNull("");//null
        Strings.emptyToNull("a");//"a"
        //Strings 还提供了常见的字符串前后拼接同一个字符直到达到某个长度，或者重复拼接自身 n 次。
        Strings.padStart("7", 3, '0');//"007"
        Strings.padStart("2010", 3, '0');//"2010"
        Strings.padEnd("4.", 5, '0');//"4.000"
        Strings.padEnd("2010", 3, '!');//"2010"
        Strings.repeat("hey", 3);//"heyheyhey"
        //Strings 的最后一组功能是查找两个字符串的公共前缀、后缀。
        Strings.commonPrefix("aaab", "aac");//"aa"
        Strings.commonSuffix("aaac", "aac");//"aac"

        String a = "CONSTANT_NAME";
        //######字符转换器
        CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, a); // returns "constantName"
        //字符匹配器
        String noControl = CharMatcher.JAVA_ISO_CONTROL.removeFrom(a); //移除control字符
        String theDigits = CharMatcher.DIGIT.retainFrom(a); //只保留数字字符
        String spaced = CharMatcher.WHITESPACE.trimAndCollapseFrom(a, ' ');
        //去除两端的空格，并把中间的连续空格替换成单个空格
        String noDigits = CharMatcher.JAVA_DIGIT.replaceFrom(a, "*"); //用*号替换所有数字
        String lowerAndDigit = CharMatcher.JAVA_DIGIT.or(CharMatcher.JAVA_LOWER_CASE).retainFrom(a);
     }
}
