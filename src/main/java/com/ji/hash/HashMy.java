package com.ji.hash;

import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/6
 * Version:1.1.0
 */
public class HashMy {
    public static void main(String[] args) {
        //校验总和哈希函数
        HashFunction adler32 = Hashing.adler32();
        HashFunction crc32 = Hashing.crc32();
        //一般哈希函数
        HashFunction gfh = Hashing.goodFastHash(128);
        HashFunction murmur3_32 = Hashing.murmur3_32();
        HashFunction murmur3_128 = Hashing.murmur3_128();
        //密码hash
        HashFunction sha1 = Hashing.sha1();
        HashFunction sha256 = Hashing.sha256();
        HashFunction sha512 = Hashing.sha512();
        //布隆过滤器
        //一致性hash
        System.out.println(Hashing.consistentHash(11212, 3));
    }
}
