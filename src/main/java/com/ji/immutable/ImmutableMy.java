package com.ji.immutable;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import java.util.List;
import java.util.Map;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/6
 * Version:1.1.0
 */
public class ImmutableMy {
    public static void main(String[] args) {
        ImmutableList<String> a=ImmutableList.of("1","2","3");
        Map<String,Integer> b=ImmutableMap.of("1",1,"2",2);

     }
}
