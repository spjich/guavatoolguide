package com.ji.order;

import com.google.common.base.Function;
import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/5
 * Version:1.1.0
 */
public class OrderMy {

    public static void main(String[] args) {
        List<People> peopleList = new ArrayList<People>() {
            {
                add(new People("A", 33));
                add(new People("B", 11));
                add(new People("C", 18));
            }
        };

        Ordering<People> ordering = Ordering.natural().onResultOf(new Function<People, Comparable>() {
            @Override
            public Comparable apply(People people) {
                return people.getAge();
            }
        });

        Ordering<People> ordering2 = Ordering.natural().reverse().onResultOf(new Function<People, Comparable>() {
            @Override
            public Comparable apply(People people) {
                return people.getAge();
            }
        });

        Ordering<People> ordering3 = Ordering.usingToString().onResultOf(new Function<People, Comparable>() {
            @Override
            public Comparable apply(People people) {
                return people.getName();
            }
        });

        Ordering<People> ordering4 = Ordering.from(new Comparator<People>() {
            @Override
            public int compare(People o1, People o2) {
                return o1.getAge() - o2.getAge();
            }
        }).reverse();


        System.out.println(ordering.sortedCopy(peopleList));
        System.out.println(ordering2.sortedCopy(peopleList));
        System.out.println(ordering3.sortedCopy(peopleList));
        System.out.println(ordering4.sortedCopy(peopleList));
    }

    private static class People {
        private String name;
        private int age;

        public People(String name, int age) {
            this.name = name;
            this.age = age;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        @Override
        public String toString() {
            return "People{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }
}
