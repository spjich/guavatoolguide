package com.ji.event;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/6
 * Version:1.1.0
 */
public class EventMy {
    public static void main(String[] args) {
        //Creates a new EventBus with the given identifier.
        EventBus eventBus = new EventBus("ricky");
        //register all subscriber
        eventBus.register(new HelloEventListener());
        //publish event
        eventBus.post(new OrderEvent("hello"));
        eventBus.post(new OrderEvent("world"));
    }

    static class OrderEvent {
        private String message;

        public OrderEvent(String message) {
            this.message = message;
        }

        public String getMessage() {
            return message;
        }
    }

    static class HelloEventListener {
        @Subscribe
        public void listen(OrderEvent event) {
            System.out.println("receive msg:" + event.getMessage());
        }
    }
}
