package com.ji.joiner;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableMap;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/5
 * Version:1.1.0
 */
public class JoinerMy {

    private static void nullSkip() {
        System.out.println(Joiner.on(' ').skipNulls().join(1, null, 3));
        System.out.println(Joiner.on(' ').useForNull("None").join(1, null, 3));

    }

    private static void separatorJoiner() {
        System.out.println(Joiner.on("#").withKeyValueSeparator("=").join(ImmutableMap.of(1, 2, 3, 4)));
     }


    public static void main(String[] args) {
        //如果我们希望忽略空指针，那么可以调用 skipNulls 方法，得到一个会跳过空指针的 Joiner 实例。
        //如果希望将空指针变为某个指定的值，那么可以调用 useForNull 方法，指定用来替换空指针的字符串。
        JoinerMy.nullSkip();

        //withKeyValueSeparator 方法指定了键与值的分隔符，同时返回一个 MapJoiner 实例。
        // 有些家伙会往 Map 里插入键或值为空指针的键值对，如果我们要拼接这种 Map，千万记得要用 useForNull 对 MapJoiner 做保护，不然 NPE 妥妥的。
        JoinerMy.separatorJoiner();
    }
}
