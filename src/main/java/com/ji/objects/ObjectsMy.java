package com.ji.objects;

import com.google.common.base.Objects;
import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Ordering;

import java.util.Date;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/5
 * Version:1.1.0
 */
public class ObjectsMy {

    private static void equalsMy() {
        //equals
        //当一个对象中的字段可以为null时，实现Object.equals方法会很痛苦，因为不得不分别对它们进行null检查。使用Objects.equal帮助你执行null敏感的equals判断，从而避免抛出NullPointerExceptio
        Objects.equal("a", "a"); // returns true
        Objects.equal(null, "a"); // returns false
        Objects.equal("a", null); // returns false
        Objects.equal(null, null); // returns true
    }


    private static void hashMy(Object o) {
        System.out.println(Objects.hashCode(o));
    }


    public static void main(String[] args) {
        equalsMy();
        hashMy(new Date());
     }


    static class Person implements Comparable<Person> {
        private int old;
        private String name;

        public int getOld() {
            return old;
        }

        public void setOld(int old) {
            this.old = old;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public int compareTo(Person that) {
            return ComparisonChain.start()
                    .compare(this.old, that.old, Ordering.<Integer>natural().nullsLast())
                    .result();
        }
    }

}
