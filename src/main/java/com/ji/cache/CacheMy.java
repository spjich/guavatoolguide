package com.ji.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2018/3/6
 * Version:1.1.0
 */
public class CacheMy {
    private static void cacheLoader() {
        LoadingCache<String, String> cache = CacheBuilder.newBuilder()
                .maximumSize(100) //最大缓存数目
                .expireAfterAccess(3, TimeUnit.SECONDS)//缓存1秒后过期
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        return key;
                    }
                });
        cache.put("j", "java");
        cache.put("c", "cpp");
        cache.put("s", "scala");
        cache.put("g", "go");
        try {
            System.out.println(cache.get("j"));
            TimeUnit.SECONDS.sleep(2);
            cache.cleanUp();
            System.out.println(cache.get("s")); //输出s
            TimeUnit.SECONDS.sleep(2);
            cache.cleanUp();
            System.out.println(cache.getIfPresent("s")); //输出s
            System.out.println(cache.get("s")); //输出s

        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }

    private static void cacheWithCallback() {
        Cache<String, String> cache = CacheBuilder.newBuilder()
                .recordStats()//增加统计
                .maximumSize(100)
                .expireAfterAccess(1, TimeUnit.SECONDS)
                .build();
        try {
            String result = cache.get("java", () -> "hello java");
            System.out.println(result);
            System.out.println(cache.get("java", () -> "hello java"));
            System.out.println(cache.stats().hitRate());

        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        //通过cacheloader构建

        //part1.  #####get()
        //get(K)这个方法返回缓存中相应的值，或者用给定的Callable运算并把结果加入到缓存中
        //getIfPresent(K) 会返回null
        CacheMy.cacheLoader();
        System.out.println("-------cache with callback");
        CacheMy.cacheWithCallback();
        //part2.  #####expire
        //expireAfterAccess(long, TimeUnit)：缓存项在给定时间内没有被读/写访问，则回收。
        //expireAfterWrite(long, TimeUnit)：缓存项在给定时间内没有被写访问（创建或覆盖），则回收。

        //part3.  ####cleanup
        //使用CacheBuilder构建的缓存不会"自动"执行清理和回收工作，也不会在某个缓存项过期后马上清理，也没有诸如此类的清理机制。相反，它会在写操作时顺带做少量的维护工作，或者偶尔在读操作时做——如果写操作实在太少的话。
        //这样做的原因在于：如果要自动地持续清理缓存，就必须有一个线程，这个线程会和用户操作竞争共享锁。此外，某些环境下线程创建可能受限制，这样CacheBuilder就不可用了。
        //相反，我们把选择权交到你手里。如果你的缓存是高吞吐的，那就无需担心缓存的维护和清理等工作。如果你的 缓存只会偶尔有写操作，而你又不想清理工作阻碍了读操作，那么可以创建自己的维护线程，以固定的时间间隔调用Cache.cleanUp()。ScheduledExecutorService可以帮助你很好地实现这样的定时调度。

        //part4.  refresh刷新
    }

}
