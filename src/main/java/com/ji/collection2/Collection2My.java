package com.ji.collection2;

import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * filter
 * transform
 */
public class Collection2My {
    public static void main(String[] args) {
        //filter
        List<String> list = Lists.newArrayList("moon", "dad", "refer", "son");
        Collection<String> palindromeList = Collections2.filter(list, input -> new StringBuilder(input).reverse().toString().equals(input));
        System.out.println(palindromeList);
        //类型转换
        Set<Long> times = Sets.newHashSet();
        times.add(91299990701L);
        times.add(9320001010L);
        times.add(9920170621L);
        Collection<String> timeStrCol = Collections2.transform(times, new SimpleDateFormat("yyyy-MM-dd")::format);
        System.out.println(timeStrCol);
        //集合操作
        Set<Integer> set1 = Sets.newHashSet(1, 2, 3, 4, 5);
        Set<Integer> set2 = Sets.newHashSet(3, 4, 5, 6);
        Sets.SetView<Integer> inter = Sets.intersection(set1, set2); //交集
        System.out.println(inter);
        Sets.SetView<Integer> diff = Sets.difference(set1, set2); //差集,在A中不在B中
        System.out.println(diff);
        Sets.SetView<Integer> union = Sets.union(set1, set2); //并集
        System.out.println(union);

    }
}
